package test

import net.evanperryg.lastkt.api.Album
import net.evanperryg.lastkt.api.Artist
import net.evanperryg.lastkt.api.Track
import net.evanperryg.lastkt.api.User
import net.evanperryg.lastkt.auth.Authenticator
import net.evanperryg.lastkt.util.Logger
import net.evanperryg.lastkt.util.forEachPair

class ApiReturnTest(private val authenticator: Authenticator): Test {
    private val referenceTrack = Track.getInfo(authenticator, name = "Let's Dance", artist = "David Bowie")
    private val referenceAlbum = Album.getInfo(authenticator, title = "Let's Dance", artist = "David Bowie")
    private val referenceArtist = Artist.getInfo(authenticator, name = "David Bowie")

    override fun run() {
        trackGetInfo()
        trackGetSimilar()
        trackSearch()

        userTopAlbums()
        userTopArtists()
        userTopTracks()

        albumGetInfo()
        albumSearch()

        artistGetInfo()
    }

    fun trackGetInfo() {
        Logger.write("")
        val (name, mbid, url, duration, listeners, playCount, artist, album, user, userLoved, userPlayCount, tags, wikiPublished, wikiSummary, wikiContent, _) = referenceTrack
        assert(name == "Let's Dance")
        assert(mbid == "666023bd-745f-4b38-bb15-325e61156fd0")
        assert(listeners != null)
        assert(playCount != null)
        assert(url != null)
        assert(duration == 248L)
        assert(artist?.name == "David Bowie")
        assert(artist?.mbid == "5441c29d-3602-4898-b1a1-b77fa23b8e50")
        assert(artist?.url != null)
        assert(album?.artist?.name == "David Bowie")
        assert(album?.title != null)
        assert(album?.url != null)
        assert(wikiPublished != null)
        assert(wikiSummary != null)
        assert(wikiContent != null)
        assert(user?.name == authenticator.username)
        assert(tags != null)

        val anotherTrack = Track.getInfo(authenticator, referenceTrack )
        assert(name == anotherTrack.name)
        assert(mbid == anotherTrack.mbid)
        assert(listeners == anotherTrack.listeners)
        assert(playCount == anotherTrack.playCount)
        assert(url == anotherTrack.url)
        assert(duration == anotherTrack.duration)
        assert(artist?.name == anotherTrack.artist?.name)
        assert(artist?.mbid == anotherTrack.artist?.mbid)
        assert(artist?.url == anotherTrack.artist?.url)
        assert(album?.artist?.name == anotherTrack.album?.artist?.name)
        assert(album?.title == anotherTrack.album?.title)
        assert(album?.url == anotherTrack.album?.url)
        assert(wikiPublished == anotherTrack.wikiPublished)
        assert(wikiSummary == anotherTrack.wikiSummary)
        assert(wikiContent == anotherTrack.wikiContent)
        assert(user?.name == anotherTrack.user?.name)
        assert(userLoved == anotherTrack.userLoved)
        assert(userPlayCount == anotherTrack.userPlayCount)
    }

    fun trackGetSimilar() {
        Logger.write("")
        Track.getSimilar(authenticator, track = referenceTrack, limit = 5).forEachPair{
            f, t ->
            assert(f > 0F)
            assert(t.name != null)
            assert(t.url != null)
            assert(t.duration != null)
            assert(t.artist?.name != null)
            assert(t.artist?.url != null)
        }
    }

    fun trackSearch() {
        Logger.write("")

        val trackList1 = Track.search(authenticator, "Let's Dance", "David Bowie", limit = 5)
        trackList1.forEach{
            assert(it.name != null)
            assert(it.artist?.name != null)
            assert(it.url != null)
            assert(it.listeners != null)
        }

        val trackList2 = Track.searchUnpaginated(authenticator, "Let's Dance", "David Bowie", 0, 5)
        trackList2.forEachIndexed{
            i, it ->
            assert(it.name == trackList1[i].name)
            assert(it.artist?.name == trackList1[i].artist?.name)
            assert(it.url == trackList1[i].url)
            assert(it.listeners == trackList1[i].listeners)
        }
    }

    fun userTopTracks() {
        Logger.write("")
        User.getTopTracks(authenticator).forEach {
            assert(it.name != null)
            assert(it.duration != null)
            assert(it.url != null)
            assert(it.artist?.name != null)
            assert(it.artist?.url != null)
            assert(it.user?.name == authenticator.username)
            assert(it.userPlayCount != null)
        }
    }

    fun userTopArtists() {
        Logger.write("")
        User.getTopArtists(authenticator).forEach {
            assert(it.name != null)
            assert(it.url != null)
            assert(it.userPlayCount != null)
            assert(it.user?.name == authenticator.username)
        }
    }

    fun userTopAlbums() {
        Logger.write("")
        User.getTopAlbums(authenticator).forEach {
            assert(it.title != null)
            assert(it.artist?.name != null)
            assert(it.artist?.url != null)
            assert(it.url != null)
            assert(it.userPlayCount != null)
        }
    }

    fun albumGetInfo() {
        Logger.write("")
        val (title, artist, url, _, _, listeners, playCount, tracks, _, user, userPlayCount, wikiPublished, wikiSummary, wikiContent) = referenceAlbum

        assert(title == "Let's Dance")
        assert(artist?.name == "David Bowie")
        assert(url != null)
        assert(listeners != null)
        assert(playCount != null)
        assert(user?.name == authenticator.username)
        assert(userPlayCount != null)
        assert(wikiPublished != null)
        assert(wikiSummary != null)
        assert(wikiContent != null)

        (tracks ?: throw AssertionError()).forEach{
            assert(it.name != null)
            assert(it.url != null)
            assert(it.duration != null)
            assert(it.artist?.name != null)
            assert(it.artist?.url != null)
        }

        assert(tracks[2].name == referenceTrack.name)
    }

    fun albumSearch() {
        Logger.write("")

        Album.search(authenticator, title = "Let's Dance", limit = 10).forEach {
            assert(it.title != null)
            assert(it.artist?.name != null)
            assert(it.url != null)
        }
    }

    fun artistGetInfo() {
        Logger.write("")
        val (name, mbid, url, image, onTour, listeners, playCount, similarArtists, tags, userPlayCount, user, bioPublished, bioSummary, bioContent) = referenceArtist

        assert(name == "David Bowie")
        assert(mbid == "5441c29d-3602-4898-b1a1-b77fa23b8e50")
        assert(url != null)
        assert(image != null)
        assert(onTour != null)
        assert(listeners != null)
        assert(playCount != null)
        assert(tags != null)
        assert(bioContent != null)
        assert(bioPublished != null)
        assert(bioSummary != null)
        assert(userPlayCount != null)
        assert(user?.name == authenticator.username)

        similarArtists?.forEach {
            assert(it.name != null)
            assert(it.url != null)
        }

    }

}