package test

import net.evanperryg.lastkt.auth.FileAuthentication
import net.evanperryg.lastkt.auth.MobileAuthentication
import net.evanperryg.lastkt.exception.LastktException

/**
 * Interface used for every class in the test package.
 */
interface Test {
    fun run()
}

/**
 * The test sequence is put here.
 */
fun main(args: Array<String>) {
    val auth = try {
        FileAuthentication("./foo.json")
    } catch (e: LastktException) {
        println("Failed to load authentication from local file!")
        println("Please enter username:")
        val username = readLine()
        println("Please enter password:")
        val password = readLine()
        MobileAuthentication(username!!, password!!, "API_KEY", "API_SECRET")
    }
    (auth as? MobileAuthentication)?.writeToFile("./foo.json")

    // tests the output of functions that return API request data, checking that it matches
    // the diagrams in the API cheat sheet
    ApiReturnTest(auth).run()
}