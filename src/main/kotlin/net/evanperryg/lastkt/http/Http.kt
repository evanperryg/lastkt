package net.evanperryg.lastkt.http

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import net.evanperryg.lastkt.exception.*
import net.evanperryg.lastkt.util.Logger
import net.evanperryg.lastkt.util.makeUTF8
import net.evanperryg.lastkt.util.readStream
import java.net.HttpURLConnection
import java.net.URL

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date 11 May 2018
 */
class Http(private val targetUrl: String) {
    var paramMap: MutableMap<String, String> = mutableMapOf()

    /**
     * @brief Puts a parameter in paramMap.
     */
    fun putParameter(key: String, value: String): Http {
        paramMap.put(key, value)
        return this
    }

    /**
     * @brief Perform an HTTP GET request.
     */
    fun get(vararg params: String): String {
        return request("GET", *params)
    }

    /**
     * @brief Perform an HTTP POST request.
     */
    fun post(vararg params: String): String {
        return request("POST", *params)
    }

    private fun request(method: String, vararg params: String): String {
        val urlString = StringBuilder("$targetUrl?")
        for(param in params) {
            if(param !in paramMap)
                throw IllegalStateException("given parameter is not mapped to a value: $param")
            urlString.append("$param=${makeUTF8(paramMap[param])}&")
        }
        //Logger.write("request to $targetUrl will be made")
        urlString.deleteCharAt(urlString.length - 1).trim()
        val url = URL(urlString.toString())
        with(url.openConnection() as HttpURLConnection) {
            requestMethod = method
            //Logger.write("response from target: $responseCode $responseMessage", System.out)
            return try {
                readStream(inputStream)
            } catch (e: Exception) {
                val errorMessage = readStream(errorStream)
                val error = (Parser().parse(rawValue = StringBuilder(errorMessage)) as JsonObject)
                        .int("error")
                val exception: Exception = when (error) {
                    4 -> LastktAuthenticationFailedException()
                    6 -> LastktInvalidParametersException()
                    10 -> LastktInvalidAPIKeyException()
                    26 -> LastktAPIKeySuspendedException()
                    29 -> LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM error with HTTP response code $responseCode: $responseMessage. Last.FM message: $errorMessage")
                }
                throw exception
            }
        }
    }
}