package net.evanperryg.lastkt.api

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import net.evanperryg.lastkt.auth.Authenticator
import net.evanperryg.lastkt.exception.*
import net.evanperryg.lastkt.http.Https
import net.evanperryg.lastkt.util.stringToDate
import java.util.*

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date   5/13/2018
 * @property name The title of the album.
 * @property mbid The musicbrainz ID for the album. Not all albums have a musicbrainz ID, so this can be null!
 * @property url The url of the album's last.fm page.
 * @property image A [List][kotlin.collections.List] of [Images](http://github.com/evanperryg/lastkt/wiki/Image) containing various sizes of the artist picture.
 * @property onTour True if this artist is currently on tour.
 * @property listeners The number of last.fm users who have listened to this album.
 * @property playCount The number of times a last.fm user has scrobbled a track on this album.
 * @property similarArtists A [List][kotlin.collections.List] containing similar artists.
 * @property tags A [List][kotlin.collections.List] containing each public [Tags](http://github.com/evanperryg/lastkt/wiki/Tag) associated with the artist.
 * @property userPlayCount The number of times the Last.fm user specified by [user] has played a track from this artist.
 * @property user The last.fm user to which [userPlayCount] refers.
 * @property bioPublished The date the biography for this artist was published.
 * @property bioSummary A short version of the artist biography.
 * @property bioContent The artist biography.
 */
data class Artist(val name: String?,
                  val mbid: String? = null,
                  val url: String? = null,
                  val image: List<Image>? = null,
                  val onTour: Boolean? = null,
                  val listeners: Long? = null,
                  val playCount: Long? = null,
                  val similarArtists: List<Artist>? = null,
                  val tags: List<Tag>? = null,
                  val userPlayCount: Long? = null,
                  val user: User? = null,
                  val bioPublished: Date? = null,
                  val bioSummary: String? = null,
                  val bioContent: String? = null) {
    companion object {

        /**
         * @brief Implements the last.fm API track.getInfo method with standard string inputs. Does not require client authentication.
         * @param authenticator Any [Authenticator].
         * @param name The name of the artist.
         * @param mbid The artist's Musicbrainz ID.
         * @param autocorrect 1 or 0 for on or off. It is highly recommended to leave this on.
         * @return An [Artist] containing the API response data.
         */
        fun getInfo(authenticator: Authenticator, name: String? = null, mbid: String? = null, autocorrect: Int = 1, username: String? = null): Artist {
            if((name == null) && (mbid == null))
                throw LastktArgumentException("One of artist or mbid must not be null!")

            // if a username is passed, we want to put the original username back into http when we're done, so hold on
            // to the current value
            val https = Https("https://ws.audioscrobbler.com/2.0/")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("sk", authenticator.sessionKey)
                    .putParameter("format", "json")
                    .putParameter("method", "artist.getInfo")
                    .putParameter("autocorrect","$autocorrect")

            val passParams = mutableListOf<String>("api_key", "autocorrect", "method", "format")

            var outUser: User? = null
            if(authenticator.username != null) {
                https.putParameter("user", authenticator.username!!)
                passParams.add("user")
                outUser = User(name = authenticator.username!!)
            }

            mapOf(Pair("artist", name), Pair("mbid", mbid), Pair("user", username))
                    .entries.forEach{(a, b) -> if(b != null) {
                if(!b.isNullOrEmpty()) {
                    https.putParameter(a, b)
                    passParams.add(a)
                    if(a == "user")
                        outUser = User(name = b)
                }
            }
            }

            val response = (Parser().parse(
                    rawValue = StringBuilder(https.post(*passParams.toTypedArray()))) as JsonObject)
            if(response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if(msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if(msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if(msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if(msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if(msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("artist")   // if there isn't an artist object, an exception was already thrown elsewhere

            val bioBlock: JsonObject? = json?.obj("bio")

            // similarArtistList will contain the list of similar artists, who'da thought
            val similarArtistList = mutableListOf<Artist>()
            json?.obj("similar")?.array<JsonObject>("artist")?.forEach {
                similarArtistList.add(
                        Artist(name   = it.string("name"),
                                url   = it.string("url"),
                                image = Image.jsonArrayToImageList(it.array<JsonObject>("image"))))
            }

            return Artist(name      = json?.string("name"),
                    mbid            = if(json?.string("mbid").isNullOrEmpty()) null
                    else json?.string("mbid"),
                    url             = json?.string("url"),
                    image           = Image.jsonArrayToImageList(json?.array("image")),
                    onTour          = json?.string("ontour")?.let{ it.toLong() > 0 },
                    listeners       = json?.obj("stats")?.string("listeners")?.toLong(),
                    playCount       = json?.obj("stats")?.string("playcount")?.toLong(),
                    user            = outUser,
                    userPlayCount   = (json?.obj("stats")?.string("userplaycount")?.toLong() ?: if(outUser == null) null else 0L),
                    similarArtists  = similarArtistList,
                    tags            = Tag.jsonArrayToTagList(json?.obj("tags")?.array("tag")),
                    bioPublished    = stringToDate(bioBlock?.string("published")),
                    bioContent      = bioBlock?.string("content"),
                    bioSummary      = bioBlock?.string("summary"))
        }

        /**
         * @brief Implements the last.fm API track.getInfo method with standard string inputs. Does not require client authentication.
         * @param authenticator Any [Authenticator].
         * @param artist The name of the artist.
         * @return An [Artist] containing the API response data.
         */
        fun getInfo(authenticator: Authenticator, artist: Artist): Artist {
            return getInfo(authenticator = authenticator, name = artist.name, autocorrect = 1)
        }
    }

    /**
     * @brief Perform the last.fm artist.getInfo method using the data stored in this [Artist](https://github.com/evanperryg/lastkt/wiki/Artist). Does not use client authentication.
     * @param authenticator Any Authenticator.
     * @return Track
     */
    fun getInfo(authenticator: Authenticator): Artist =
            Artist.getInfo(authenticator, this)

    /**
     * @brief Iterate through each Artist in this.similarArtists, performing the artist.getInfo method on each artist in the iteration.
     * @throws LastktStateException Thrown if this.similarArtists is null.
     * @param action Function1<Artist, Unit>
     */
    inline fun forEachSimilarArtist(authenticator: Authenticator, action: (Artist) -> Unit) {
        if(similarArtists == null) throw LastktStateException("Artist.forEachSimilarArtist cannot be called on an Artist with a null similarArtists property!")
        else for(artist in similarArtists) action(Artist.getInfo(authenticator, artist))
    }

    /**
     * @brief Iterate through each of the Artist's tags.
     * @throws LastktStateException Thrown if this.tags is null.
     * @param action Function1<Tag, Unit>
     * @return Unit
     */
    inline fun forEachTag(action: (Tag) -> Unit) {
        if (tags == null) throw LastktStateException("Artist.forEachTag cannot be called on an Artist with a null tags property!")
        else return tags.forEach(action)
    }

    override fun toString(): String = "Artist(name=${name ?: "null"}, mbid=${mbid ?: "null"}, image=${image ?: "null"}, " +
            "onTour=${onTour ?: "null"}, listeners=${listeners ?: "null"}, playCount=${playCount ?: "null"}, " +
            "similarArtists=${similarArtists ?: "null"}, tags=${tags ?: "null"}, bioPublished=${bioPublished ?: "null"}, " +
            "bioSummary=${bioSummary ?: "null"}, bioContent=${bioContent ?: "null"})"
}