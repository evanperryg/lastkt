package net.evanperryg.lastkt.api

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import net.evanperryg.lastkt.api.helper.Period
import net.evanperryg.lastkt.auth.Authenticator
import net.evanperryg.lastkt.exception.*
import net.evanperryg.lastkt.http.Http


/**
 * @brief A data model representing a Last.fm user.
 * @author evan
 * @date   5/14/2018
 * @property name The user's Last.fm username.
 * @property url  A link to the user's Last.fm profile.
 * @property image A [List][kotlin.collections.List] of various sizes of the user's profile [Image].
 * @property country A String containing the user's country. If the user has not specified a country on their profile, this will be null.
 * @property isSubscriber True if the user is a last.fm paid subscriber, false otherwise.
 * @property playCount The total number of scrobbles on this user's account.
 */
data class User(val name: String,
           val url: String? = null,
           val image: List<Image>? = null,
           val country: String? = null,
           val isSubscriber: Boolean? = null,
           val playCount: Long? = null) {
    companion object {
        /**
         * @brief A binding for the user.getInfo method. Does not require client authentication.
         * @param authenticator Any [Authenticator], does not need client authorization.
         * @param username The username to retrieve information for. If this parameter is not provided, search for the username contained by [authenticator].
         * @return If the API request is successful, returns a User object. See the API Cheat Sheet wiki page for detail on which fields will be filled.
         */
        fun getInfo(authenticator: Authenticator, username: String? = null): User {
            if((username == null) && (authenticator.username == null))
                throw LastktArgumentException("User.getInfo must be provided with a username parameter, or an authenticator with a valid username!")

            val http = Http("http://ws.audioscrobbler.com/2.0/")
                    .putParameter("method", "user.getInfo")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("user", username ?: authenticator.username!!)
                    .putParameter("format", "json")

            val response = (Parser().parse(
                    rawValue = StringBuilder(http.post("method", "api_key", "user", "format"))) as JsonObject)
            if(response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if(msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if(msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if(msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if(msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if(msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("user")

            return User(name    = json?.string("name")!!,
                    url     = json.string("url"),
                    image   = Image.jsonArrayToImageList(json.array("image")),
                    country = if(json.string("country")!!.isEmpty()) null else json.string("country")!!,
                    isSubscriber = json.string("subscriber")?.equals("1")!!,
                    playCount = json.string("playcount")!!.toLong())
        }

        /**
         * @brief A binding for the paginated user.getLovedTracks method. Does not require client authentication.
         * @param authenticator Any [Authenticator], does not need client authorization.
         * @param username The username whose loved tracks we will return. If this parameter is not provided, search the username contained by [authenticator].
         * @param page The page number of tracks to get. Defaults to the first (most recent) page.
         * @param limit The maximum number of results to retrieve, per page. Default is 50.
         */
        fun getLovedTracks(authenticator: Authenticator, username: String? = null, page: Int = 1, limit: Int = 50): List<Track> {
            if((username == null) && (authenticator.username == null))
                throw LastktArgumentException("User.getInfo must be provided with a username parameter, or an authenticator with a valid username!")

            val outUser = if(username != null) User(name = username) else User(name = authenticator.username!!)

            val http = Http("http://ws.audioscrobbler.com/2.0/")
                    .putParameter("method", "user.getLovedTracks")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("user", username ?: authenticator.username!!)
                    .putParameter("page", "$page")
                    .putParameter("limit", "$limit")
                    .putParameter("format", "json")

            val response = (Parser().parse(
                    rawValue = StringBuilder(http.post("method", "api_key", "user", "page", "limit", "format"))) as JsonObject)
            if(response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if(msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if(msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if(msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if(msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if(msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("lovedtracks")?.array<JsonObject>("track")!!

            if (json.isEmpty()) return listOf()

            val trackList = mutableListOf<Track>()
            json.forEach {
                trackList.add(Track(name = it.string("name"),
                        mbid = if(it.string("mbid").isNullOrEmpty()) null
                        else it.string("mbid"),
                        url  = it.string("url"),
                        user = outUser,
                        userLoved = true,
                        artist = Artist(name = it.obj("artist")?.string("name"),
                                mbid = if(it.obj("artist")?.string("mbid").isNullOrEmpty()) null
                                else it.obj("artist")?.string("mbid"),
                                url  = it.obj("artist")?.string("url"))))
            }

            return trackList
        }

        /**
         * @brief Perform a user.getLovedTracks request, but with unpaginated input parameters.
         *
         * This method may perform multiple user.getLovedTracks requests in order to produce its response.
         *
         * @param authenticator Any [Authenticator], does not need client authorization.
         * @param username The username whose loved tracks we will return. If this parameter is not provided, search the username contained by [authenticator].
         * @param startIndex The index of the first loved track to return. Index 0 is the most recently loved track.
         * @param endIndex One plus the index of the last loved track to return. A track will not be appended to the list for any index beyond the user's oldest loved track.
         */
        fun getLovedTracksUnpaginated(authenticator: Authenticator, username: String? = null, startIndex: Int, endIndex: Int): List<Track> {
            if((username == null) && (authenticator.username == null))
                throw LastktArgumentException("User.getInfo must be provided with a username parameter, or an authenticator with a valid username!")
            if((startIndex > endIndex))
                return listOf()

            val maxOutputLength: Int = endIndex - startIndex
            when {
                startIndex == 0 -> {
                    val retrievePerPage = if(maxOutputLength > 1000) 1000 else maxOutputLength
                    val trackList = mutableListOf<Track>()

                    var page = 1
                    var lastSize: Int
                    while(trackList.size < maxOutputLength) {
                        lastSize = trackList.size
                        trackList.addAll(getLovedTracks(authenticator, username, page, retrievePerPage))
                        if(trackList.size == lastSize) return trackList
                        page++
                    }
                    return trackList
                }
                endIndex <= 50 -> // allows all calls with an endIndex <= 50 to take only one API call
                    return getLovedTracks(authenticator, username).filterIndexed{ index, _ -> ((index >= startIndex) && (index < endIndex))}
                else -> {
                    // first one -> page = 2, limit = startIndex
                    val trackLs = mutableListOf<Track>()
                    val limit = if(startIndex > 1000) 1000 else startIndex
                    val page = (1 + (startIndex / limit))
                    val firstIndex = startIndex % ((page - 1) * limit)
                    val result = getLovedTracks(authenticator, username, page, limit)
                    trackLs.addAll(result.filterIndexed{ index, _ -> (index >= firstIndex) })

                    // if we're bigger than max length, trim down the end and return
                    if(trackLs.size >= maxOutputLength)
                        return trackLs.filterIndexed{ index,_ -> index < maxOutputLength }

                    // if the size of trackLs is less than (page * limit) - lastIndex, we reached the end of the user's
                    // loved tracks, so just return what we have
                    if(result.size < limit)
                        return trackLs

                    // if we haven't returned yet, that means we need to add more stuff to trackLs. Do it recursively:
                    trackLs.addAll(getLovedTracksUnpaginated(authenticator, username, startIndex + trackLs.size, endIndex))
                    return trackLs
                }
            }
        }

        /**
         * @brief Perform a user.getTopTracks request.
         * @param authenticator Any [Authenticator], does not need client authorization.
         * @param username The username whose top tracks should be returned. If this parameter is not provided, the username contained in [authenticator] will be used.
         * @param period A Period specifying the time period over which the method will return data. Defaults to all-time (overall)
         * @param page The page number to fetch. Defaults to the first page (which would be the highest of the top tracks)
         * @param limit The number of results to retrieve per page. Defaults to 50.
         */
        fun getTopTracks(authenticator: Authenticator, username: String? = null, period: Period = Period.OVERALL, page: Int = 1, limit: Int = 50): List<Track> {
            val user = User(name = username ?: authenticator.username ?:
                throw LastktArgumentException("User.getInfo must be provided with a username parameter, or an authenticator with a valid username!"))

            val http = Http("http://ws.audioscrobbler.com/2.0/")
                    .putParameter("method", "user.getTopTracks")
                    .putParameter("period", period.string)
                    .putParameter("limit", "$limit")
                    .putParameter("page", "$page")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("user", user.name)
                    .putParameter("format", "json")

            val response = (Parser().parse(
                    rawValue = StringBuilder(http.post("method", "api_key", "user", "page", "limit", "format"))) as JsonObject)
            if(response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    6 -> if(msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if(msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if(msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if(msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("toptracks")?.array<JsonObject>("track")!!

            val trackList = mutableListOf<Track>()
            json.forEach {
                val artistInfo = it.obj("artist")

                trackList.add(Track(name = it.string("name"),
                                    duration = it.string("duration")?.toLong(),
                                    userPlayCount = it.string("playcount")?.toLong(),
                                    mbid = if(it.string("mbid").isNullOrEmpty()) null else it.string("mbid"),
                                    url = it.string("url"),
                                    user = user,
                                    artist = Artist(name = artistInfo?.string("name"),
                                                    mbid = if(artistInfo?.string("mbid").isNullOrEmpty()) null else artistInfo?.string("mbid"),
                                                    url = artistInfo?.string("url"))))
            }

            return trackList
        }

        /**
         * @brief Perform the user.getTopTracks method, but with non-paginated input parameters.
         *
         * This method may make more than one request to the Last.FM API!
         *
         * @param authenticator Any [Authenticator], does not need client authorization.
         * @param username The username whose top tracks should be returned. If this parameter is not provided, the username contained in [authenticator] will be used.
         * @param period A Period specifying the time period over which the method will return data. Defaults to all-time (overall)
         * @param startIndex The index of the first track to return, 0 being the highest top-played track.
         * @param endIndex One plus the index of the last track to return.
         */
        fun getTopTracksUnpaginated(authenticator: Authenticator, username: String? = null, period: Period = Period.OVERALL, startIndex: Int, endIndex: Int): List<Track> {
            if((username == null) && (authenticator.username == null))
                throw LastktArgumentException("User.getTopTracksUnpaginated must be provided with a username parameter, or an authenticator with a valid username!")
            if((startIndex > endIndex))
                return listOf()

            val maxOutputLength: Int = endIndex - startIndex
            when {
                startIndex == 0 -> {
                    val retrievePerPage = if(maxOutputLength > 1000) 1000 else maxOutputLength
                    val trackList = mutableListOf<Track>()

                    var page = 1
                    var lastSize: Int
                    while(trackList.size < maxOutputLength) {
                        lastSize = trackList.size
                        trackList.addAll(getTopTracks(authenticator, username, period, page, retrievePerPage))
                        if(trackList.size == lastSize) return trackList
                        page++
                    }
                    return trackList
                }
                endIndex <= 50 -> // allows all calls with an endIndex <= 50 to take only one API call
                    return getTopTracks(authenticator, username, period).filterIndexed{ index, _ -> ((index >= startIndex) && (index < endIndex))}
                else -> {
                    // first one -> page = 2, limit = startIndex
                    val trackLs = mutableListOf<Track>()
                    val limit = if(startIndex > 1000) 1000 else startIndex
                    val page = (1 + (startIndex / limit))
                    val firstIndex = startIndex % ((page - 1) * limit)
                    val result = getTopTracks(authenticator, username, period, page, limit)
                    trackLs.addAll(result.filterIndexed{ index, _ -> (index >= firstIndex) })

                    // if we're bigger than max length, trim down the end and return
                    if(trackLs.size >= maxOutputLength)
                        return trackLs.filterIndexed{ index, _ -> index < maxOutputLength }

                    // if the result was smaller than the specified limit, we've reached the end of the user's top tracks
                    if(result.size < limit)
                        return trackLs

                    // if we haven't returned yet, that means we need to add more stuff to trackLs. Do it recursively:
                    trackLs.addAll(getTopTracksUnpaginated(authenticator, username, period, startIndex + trackLs.size, endIndex))
                    return trackLs
                }
            }
        }

        /**
         * @brief Perform the user.getTopArtists method.
         * @param authenticator Any [Authenticator], does not need client authorization.
         * @param username The username whose top artists shoul be returned. If this parameter is not provided, the username contained in [authenticator] will be used.
         * @param period A Period specifying the time period over which the method will return data. Defaults to all-time (overall)
         * @param page The page number to fetch. Defaults to the first page (which would be the highest of the top artists)
         * @param limit The number of results to retrieve per page. Defaults to 50.
         */
        fun getTopArtists(authenticator: Authenticator, username: String? = null, period: Period = Period.OVERALL, page: Int = 1, limit: Int = 50): List<Artist> {
            val user = User(name = username ?: authenticator.username ?:
                throw LastktArgumentException("User.getInfo must be provided with a username parameter, or an authenticator with a valid username!"))

            val http = Http("http://ws.audioscrobbler.com/2.0/")
                    .putParameter("method", "user.getTopArtists")
                    .putParameter("user", user.name)
                    .putParameter("period", period.string)
                    .putParameter("limit", "$limit")
                    .putParameter("page", "$page")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("format", "json")

            val response = (Parser().parse(
                    rawValue = StringBuilder(http.post("method", "api_key", "user", "page", "limit", "format"))) as JsonObject)
            if(response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    6 -> if(msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if(msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if(msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if(msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("topartists")?.array<JsonObject>("artist")
            val artistList = mutableListOf<Artist>()

            json?.forEach {
                artistList.add(Artist(name = it.string("name"),
                                      url = it.string("url"),
                                      userPlayCount = it.string("playcount")?.toLong(),
                                      user = user,
                                      mbid = if(it.string("mbid").isNullOrEmpty()) null else it.string("mbid"),
                                      image = Image.jsonArrayToImageList(it.array<JsonObject>("image"))))
            }
            return artistList
        }

        /**
         * @brief Perform the user.getTopArtists method, but with non-paginated input parameters.
         * @param authenticator Any [Authenticator], does not need client authorization.
         * @param username The username whose top artists should be returned. If this parameter is not provided, the username contained in [authenticator] will be used.
         * @param period A Period specifying the time period over which the method will return data. Defaults to all-time (overall)
         * @param startIndex The index of the first artist to return, 0 being the highest top-played artist.
         * @param endIndex One plus the index of the last artist to return.
         */
        fun getTopArtistsUnpaginated(authenticator: Authenticator, username: String? = null, period: Period = Period.OVERALL, startIndex: Int, endIndex: Int): List<Artist> {
            if((username == null) && (authenticator.username == null))
                throw LastktArgumentException("User.getTopArtistsUnpaginated must be provided with a username parameter, or an authenticator with a valid username!")
            if((startIndex > endIndex))
                return listOf()

            val maxOutputLength: Int = endIndex - startIndex
            when {
                startIndex == 0 -> {
                    val retrievePerPage = if(maxOutputLength > 1000) 1000 else maxOutputLength
                    val artistList = mutableListOf<Artist>()

                    var page = 1
                    var lastSize: Int
                    while(artistList.size < maxOutputLength) {
                        lastSize = artistList.size
                        artistList.addAll(getTopArtists(authenticator, username, period, page, retrievePerPage))
                        if(artistList.size == lastSize) return artistList
                        page++
                    }
                    return artistList
                }
                endIndex <= 50 -> // allows all calls with an endIndex <= 50 to take only one API call
                    return getTopArtists(authenticator, username, period).filterIndexed{ index, _ -> ((index >= startIndex) && (index < endIndex))}
                else -> {
                    // first one -> page = 2, limit = startIndex
                    val artistLs = mutableListOf<Artist>()
                    val limit = if(startIndex > 1000) 1000 else startIndex
                    val page = (1 + (startIndex / limit))
                    val firstIndex = startIndex % ((page - 1) * limit)
                    val result = getTopArtists(authenticator, username, period, page, limit)
                    artistLs.addAll(result.filterIndexed{ index, _ -> (index >= firstIndex) })

                    // if we're bigger than max length, trim down the end and return
                    if(artistLs.size >= maxOutputLength)
                        return artistLs.filterIndexed{ index, _ -> index < maxOutputLength }

                    // if the result was smaller than the specified limit, we've reached the end of the user's top tracks
                    if(result.size < limit)
                        return artistLs

                    // if we haven't returned yet, that means we need to add more stuff to artistLs. Do it recursively:
                    artistLs.addAll(getTopArtistsUnpaginated(authenticator, username, period, startIndex + artistLs.size, endIndex))
                    return artistLs
                }
            }
        }

        /**
         * @brief Perform the user.getTopAlbums method.
         * @param authenticator Any [Authenticator], does not need client authorization.
         * @param username The username whose top albums should be returned. If this parameter is not provided, the username contained in [authenticator] will be used.
         * @param period A Period specifying the time period over which the method will return data. Defaults to all-time (overall)
         * @param page The page number to fetch. Defaults to the first page (which would be the highest of the top albums)
         * @param limit The number of results to retrieve per page. Defaults to 50.
         */
        fun getTopAlbums(authenticator: Authenticator, username: String? = null, period: Period = Period.OVERALL, page: Int = 1, limit: Int = 50): List<Album> {
            val user = User(name = username ?: authenticator.username
                ?: throw LastktArgumentException("User.getInfo must be provided with a username parameter, or an authenticator with a valid username!"))

            val http = Http("http://ws.audioscrobbler.com/2.0/")
                    .putParameter("method", "user.getTopalbums")
                    .putParameter("period", period.string)
                    .putParameter("limit", "$limit")
                    .putParameter("page", "$page")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("user", user.name)
                    .putParameter("format", "json")

            val response = (Parser().parse(
                    rawValue = StringBuilder(http.post("method", "api_key", "user", "page", "limit", "format"))) as JsonObject)
            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("topalbums")?.array<JsonObject>("album")
            val albumList = mutableListOf<Album>()

            json?.forEach {
                val artistInfo = it.obj("artist")
                albumList.add(Album(title = it.string("name"),
                                    user = user,
                                    userPlayCount = it.string("playcount")?.toLong(),
                                    mbid = if(it.string("mbid").isNullOrEmpty()) null else it.string("mbid"),
                                    url = it.string("url"),
                                    artist = Artist(name = artistInfo?.string("name"),
                                                    mbid = if(artistInfo?.string("mbid").isNullOrEmpty()) null else artistInfo?.string("mbid"),
                                                    url = artistInfo?.string("url")),
                                    image = Image.jsonArrayToImageList(it.array("image"))))
            }
            return albumList
        }

        /**
         * @brief Perform the user.getTopAlbums method, but with non-paginated input parameters.
         * @param authenticator Any [Authenticator], does not need client authorization.
         * @param username The username whose top albums should be returned. If this parameter is not provided, the username contained in [authenticator] will be used.
         * @param period A Period specifying the time period over which the method will return data. Defaults to all-time (overall)
         * @param startIndex The index of the first album to return, 0 being the highest top-played artist.
         * @param endIndex One plus the index of the last artist to return.
         */
        fun getTopAlbumsUnpaginated(authenticator: Authenticator, username: String? = null, period: Period = Period.OVERALL, startIndex: Int, endIndex: Int): List<Album> {
            if((username == null) && (authenticator.username == null))
                throw LastktArgumentException("User.getTopAlbumsUnpaginated must be provided with a username parameter, or an authenticator with a valid username!")
            if((startIndex > endIndex))
                return listOf()

            val maxOutputLength: Int = endIndex - startIndex
            when {
                startIndex == 0 -> {
                    val retrievePerPage = if(maxOutputLength > 1000) 1000 else maxOutputLength
                    val albumList = mutableListOf<Album>()

                    var page = 1
                    var lastSize: Int
                    while(albumList.size < maxOutputLength) {
                        lastSize = albumList.size
                        albumList.addAll(getTopAlbums(authenticator, username, period, page, retrievePerPage))
                        if(albumList.size == lastSize) return albumList
                        page++
                    }
                    return albumList
                }
                endIndex <= 50 -> // allows all calls with an endIndex <= 50 to take only one API call
                    return getTopAlbums(authenticator, username, period).filterIndexed{ index, _ -> ((index >= startIndex) && (index < endIndex))}
                else -> {
                    // first one -> page = 2, limit = startIndex
                    val albumLs = mutableListOf<Album>()
                    val limit = if(startIndex > 1000) 1000 else startIndex
                    val page = (1 + (startIndex / limit))
                    val firstIndex = startIndex % ((page - 1) * limit)
                    val result = getTopAlbums(authenticator, username, period, page, limit)
                    albumLs.addAll(result.filterIndexed{ index, _ -> (index >= firstIndex) })

                    // if we're bigger than max length, trim down the end and return
                    if(albumLs.size >= maxOutputLength)
                        return albumLs.filterIndexed{ index, _ -> index < maxOutputLength }

                    // if the result was smaller than the specified limit, we've reached the end of the user's top tracks
                    if(result.size < limit)
                        return albumLs

                    // if we haven't returned yet, that means we need to add more stuff to albumLs. Do it recursively:
                    albumLs.addAll(getTopAlbumsUnpaginated(authenticator, username, period, startIndex + albumLs.size, endIndex))
                    return albumLs
                }
            }
        }
    }

    /**
     * @brief Performs the given action if the [isSubscriber] field is true.
     * @throws LastktStateException Thrown if the isSubscriber field is null.
     * @param action Function1<User, Unit>
     * @return Unit
     */
    inline fun ifSubscriber(action: (User) -> Unit): Unit
            = if(isSubscriber ?: throw LastktStateException("inline function User.ifSubscriber cannot be called if User.isSubscriber is null!")) action(this) else Unit

    /**
     * @brief Performs the given action if the [isSubscriber] field is false.
     * @throws LastktStateException Thrown if the isSubscriber field is null.
     * @param action Function1<User, Unit>
     * @return Unit
     */
    inline fun ifNotSubscriber(action: (User) -> Unit): Unit
            = if(isSubscriber ?: throw LastktStateException("inline function User.ifSubscriber cannot be called if User.isSubscriber is null!")) Unit else action(this)

    override fun toString(): String = "User(name=$name, url=${url ?: "null"}, image=${image ?: "null"}, " +
            "country=${country ?: "null"}, isSubscriber=${isSubscriber ?: "null"}, playCount=${playCount ?: "null"})"
}