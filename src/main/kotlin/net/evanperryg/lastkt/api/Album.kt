package net.evanperryg.lastkt.api

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import net.evanperryg.lastkt.auth.Authenticator
import net.evanperryg.lastkt.exception.*
import net.evanperryg.lastkt.http.Https
import net.evanperryg.lastkt.util.doIfNotNull
import net.evanperryg.lastkt.util.stringToDate
import java.util.Date

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date   5/13/2018
 * @property title The title of the album.
 * @property artist An [Artist] that contains the album artist.
 * @property url The url of the album's last.fm page.
 * @property mbid The musicbrainz ID for the album. Not all albums have a musicbrainz ID, so this can be null!
 * @property image A [kotlin.collections.List] of [Image]s containing various sizes of the album artwork.
 * @property listeners The number of last.fm users who have listened to this album.
 * @property playCount The number of times a last.fm user has scrobbled a track on this album.
 * @property tracks A [kotlin.collections.List] containing each [Track] on the album.
 * @property tags A [kotlin.collections.List] containing each public [Tag] associated with the album.
 * @property user The last.fm user account to which [userPlayCount] refers.
 * @property userPlayCount The number of times the specified user has played a track on this album.
 * @property wikiPublished The date the wiki entry for this album was published.
 * @property wikiSummary A short version of the wiki entry for this album.
 * @property wikiContent The wiki entry for this album.
 */
data class Album(val title: String?,
                 val artist: Artist?,
                 val url: String?,
                 val mbid: String? = null,
                 val image: List<Image>? = null,
                 val listeners: Long? = null,
                 val playCount: Long? = null,
                 val tracks: List<Track>? = null,
                 val tags: List<Tag>? = null,
                 val user: User? = null,
                 val userPlayCount: Long? = null,
                 val wikiPublished: Date? = null,
                 val wikiSummary: String? = null,
                 val wikiContent: String? = null) {
    companion object {
        /**
         * @brief Implementation of last.fm album.getInfo method. Does not require authentication.
         * @param authenticator Any Authenticator.
         * @param title String? The title of the album. Optional if [mbid] is included.
         * @param artist String? The name of the album artist. Optional if [mbid] is included.
         * @param mbid String? The Musicbrainz ID for this album. Optional if both [title] and [artist] are included.
         * @param autocorrect Int last.fm's autocorrect utility can be turned of by setting this to 0 (not recommended)
         * @param username String? A last.fm user to find this albums's play history for. If this field is not provided, the returned [user] and [userPlayCount] properties will contain information pertaining to the last.fm user stored in [authenticator], or these properties will be null if no last.fm username is found in [authenticator].
         * @return Album containing all of the retrieved data from last.fm.
         */
        fun getInfo(authenticator: Authenticator, title: String? = null, artist: String? = null, mbid: String? = null, autocorrect: Int = 1, username: String? = null): Album {
            if((title == null || artist == null) && (mbid == null))
                throw LastktArgumentException("Both title AND artist must be supplied, OR mbid may be provided, making track and artist each optional.")

            val https = Https("https://ws.audioscrobbler.com/2.0/")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("sk", authenticator.sessionKey)
                    .putParameter("format", "json")
                    .putParameter("method", "album.getInfo")
                    .putParameter("autocorrect","$autocorrect")

            val passParams = mutableListOf<String>("api_key", "autocorrect", "method", "format")

            var outUser: User? = null
            if(authenticator.username != null) {
                https.putParameter("user", authenticator.username!!)
                passParams.add("user")
                outUser = User(name = authenticator.username!!)
            }

            mapOf(Pair("album", title), Pair("artist", artist), Pair("mbid", mbid), Pair("user", username))
                    .entries.forEach{(a, b) -> if(b != null) {
                if(b.isNotEmpty()) {
                    https.putParameter(a, b)
                    passParams.add(a)
                    if(a == "user")
                        outUser = User(name = b)
                }
            }
            }

            val response = (Parser().parse(
                    rawValue = StringBuilder(https.post(*passParams.toTypedArray()))) as JsonObject)
            if(response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if(msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if(msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if(msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if(msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if(msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("album")   // if there isn't an title object, an exception was already thrown elsewhere

            val wikiBlock: JsonObject? = json?.obj("wiki")

            val trackList = mutableListOf<Track>()          // trackList will contain the List<> of Tracks, go figure
            json?.obj("tracks")?.array<JsonObject>("track")?.forEach{
                trackList.add(Track(name = it.string("name")!!,
                        url = it.string("url")!!,
                        duration = it.string("duration")?.toLong(),
                        artist = Artist(name = it.obj("artist")!!.string("name"),
                                mbid = if(it.string("mbid").isNullOrEmpty()) null
                                else it.string("mbid"),
                                url  = it.obj("artist")!!.string("url"))))
            }

            return Album(title  = json?.string("name"),
                    mbid        = if(json?.string("mbid").isNullOrEmpty()) null
                    else json?.string("mbid"),
                    artist      = Artist(name = json?.string("artist")),
                    url         = json?.string("url"),
                    image       = Image.jsonArrayToImageList(json?.array<JsonObject>("image")),
                    listeners   = json?.string("listeners")?.toLong(),
                    playCount   = json?.string("playcount")?.toLong(),
                    tracks      = trackList,
                    tags        = Tag.jsonArrayToTagList(json?.obj("tags")?.array("tag")),
                    user        = outUser,
                    userPlayCount = (json?.string("userplaycount")?.toLong() ?: if(outUser == null) null else 0L),
                    wikiPublished = stringToDate(wikiBlock?.string("published")),
                    wikiSummary = wikiBlock?.string("summary"),
                    wikiContent = wikiBlock?.string("content"))
        }

        fun search(authenticator: Authenticator, title: String, limit: Int = 30, page: Int = 1): List<Album> {
            val https = Https("https://ws.audioscrobbler.com/2.0/")
                    .putParameter("method", "album.search")
                    .putParameter("album", title)
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("limit", "$limit")
                    .putParameter("page", "$page")
                    .putParameter("format", "json")

            val response = (Parser().parse(rawValue = StringBuilder(https.post("api_key", "album", "method", "limit", "page", "format"))) as JsonObject)
            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("results")?.obj("albummatches")?.array<JsonObject>("album")
            val out = mutableListOf<Album>()

            json?.forEach {

                out.add(Album(title = it.string("name"),
                              artist = Artist(name = it.string("artist")),
                              url = it.string("url"),
                              mbid = it.string("mbid"),
                              image = Image.jsonArrayToImageList(it.array("image"))))
            }
            return out
        }
    }

    /**
     * @brief Perform the last.fm album.getInfo method using the data stored in this [Album]. Does not use client authentication.
     *
     * If this [Album]'s [user] property is not null, the returned
     * Album's [user] and [userPlayCount] property will pertain to this Album's [user] property. If this Album's [user]
     * property is null, the returned Album's [user] and [userPlayCount] properties will pertain to the username found
     * in [authenticator]. If no last.fm username is found in this or [authenticator], the returned Album's [user]
     * and [userPlayCount] properties will be null.
     *
     * @param authenticator Any Authenticator.
     * @return Album with the results from last.fm's album.getInfo method.
     */
    fun getInfo(authenticator: Authenticator): Album
            = Album.getInfo(authenticator, title = title, artist = artist?.name, mbid = mbid, username = user?.name)

    /**
     * @brief Iterate through each track in the album, performing a track.getInfo API call on each track in the iteration.
     * @throws LastktStateException Thrown if Album.tracks is null.
     * @param  action Function1<Track, Unit>
     * @return Unit
     */
    inline fun forEachTrack(authenticator: Authenticator, action: (Track) -> Unit): Unit {
        if (tracks == null) throw LastktStateException("Album.forEachTrack cannot be called on an Album with a null track property!")
        else for (track in tracks) action(Track.getInfo(authenticator, track))
    }

    /**
     * @brief Iterate through each track in the album, performing a track.getInfo API call on each track in the iteration. including an index number.
     * @throws LastktStateException Thrown if Album.tracks is null.
     * @param  action Function2<Int, Track, Unit>
     * @return Unit
     */
    inline fun forEachTrackIndexed(authenticator: Authenticator, action: (index: Int, track: Track) -> Unit): Unit {
        var index = 0
        if (tracks == null) throw LastktStateException("Album.forEachTrackIndexed cannot be called on an Album with a null track property!")
        else for (track in tracks) action(index++, Track.getInfo(authenticator, track))
    }

    /**
     * @brief Iterate through each of the album's tags, performing a tag.getInfo API call on each track in the iteration. .
     * @throws LastktStateException Thrown if Album.tags is null.
     * @param  action Function1<Tag, Unit>
     * @return Unit
     */
    inline fun forEachTag(action: (Tag) -> Unit) {
        if (tags == null) throw LastktStateException("Album.forEachTag cannot be called on an Album with a null tags property!")
        else return tags.forEach(action)
    }

    override fun toString(): String = "Album(title=${title ?: "null"}, artist=${artist ?: "null"}, url=${url ?: "null"}, " +
            "mbid=${mbid ?: "null"}, image=${image ?: "null"}, listeners=${listeners ?: "null"}, playCount=${playCount ?: "null"}, " +
            "tracks=${tracks ?: "null"}, tags=${tags ?: "null"}, user=${user ?: "null"}, userPlayCount=${userPlayCount ?: "null"}, " +
            "wikiPublished=${wikiPublished ?: "null"}, wikiSummary=${wikiSummary ?: "null"}, wikiContent=${wikiContent ?: "null"})"
}