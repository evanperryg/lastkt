package net.evanperryg.lastkt.api

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import net.evanperryg.lastkt.auth.Authenticator
import net.evanperryg.lastkt.auth.NoAuthentication
import net.evanperryg.lastkt.exception.*
import net.evanperryg.lastkt.http.Http
import net.evanperryg.lastkt.http.Https
import net.evanperryg.lastkt.util.Logger
import net.evanperryg.lastkt.util.doIfNotNull
import net.evanperryg.lastkt.util.md5
import net.evanperryg.lastkt.util.stringToDate
import java.util.Date

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @brief  Data model representing a last.fm Track.
 * @author Evan Perry Grove
 * @date   5/12/2018
 *
 * The track data class lays out the basic structure of a Last.FM track, and also implements all methods in the
 * Last.FM API that begin with the Track prefix (e.g. track.addTags, track.getInfo). Any time a method returns a
 * data structure which represents a single song, it will be wrapped in a Track.
 *
 * @property name The name of the track.
 * @property mbid The musicbrainz ID of the track. Some songs don't have an mbid, so this paramater may be null.
 * @property url  A link to the last.fm page for this track.
 * @property duration The duration of the track in seconds.
 * @property listeners The number of last.fm users who have scrobbled this track.
 * @property playCount The number of times this track has been scrobbled by last.fm users.
 * @property artist An [Artist](https://github.com/evanperryg/lastkt/wiki/Artist) structure containing any information about the artist associated with this track.
 * @property album An [Album](https://github.com/evanperryg/lastkt/wiki/Album) structure containing any information about the album this track appears on.
 * @property user A last.fm user in the form of a [User](https://github.com/evanperryg/lastkt/wiki/User) data class instance. [user] specifies the last.fm user to which the [userLoved] and [userPlayCount] properties refer.
 * @property userLoved If the last.fm user specified by [user] has loved this [Track](https://github.com/evanperryg/lastkt/wiki/Track).
 * @property userPlayCount The number of times the last.fm user specified by [user] has played this [Track](https://github.com/evanperryg/lastkt/wiki/Track).
 * @property tags A [kotlin.collections.List] of [Tags](https://github.com/evanperryg/lastkt/wiki/Tag) associated with this track.
 * @property wikiPublished The date the wiki entry for this song was published.
 * @property wikiSummary A short version of the wiki entry for this track.
 * @property wikiContent The wiki entry for this track.
 * @property timestamp If defined, represents the time the track started playing, represented as Unix UTC time in seconds.
 */
data class Track(val name: String?,
                 val mbid: String? = null,
                 val url: String?,
                 val duration: Long? = null,
                 val listeners: Long? = null,
                 val playCount: Long? = null,
                 val artist: Artist?,
                 val album: Album? = null,
                 val user: User? = null,
                 val userLoved: Boolean? = null,
                 val userPlayCount: Long? = null,
                 val tags: List<Tag>? = null,
                 val wikiPublished: Date? = null,
                 val wikiSummary: String? = null,
                 val wikiContent: String? = null,
                 val timestamp: Long? = null) {

    companion object {

        /**
         * @brief Implements the track.addTags method. Requires client authentication.
         * @param authenticator An [Authenticator] with session key.
         * @param artist The artist's name for the track we want to tag.
         * @param name The name of the track we want to tag.
         * @param tags A comma-delimited list of tags to write to the specified track.
         */
        fun addTags(authenticator: Authenticator, artist: String, name: String, vararg tags: String) {
            if (authenticator is NoAuthentication)
                throw LastktArgumentException("track.addTags requires client authorization, given Authenticator does not have client authorization!")
            if (tags.isEmpty() || tags.size > 10)
                throw LastktArgumentException("Invalid number of tags! (must be greater than 0 and less than 10)")
            else {
                val https = Https("https://ws.audioscrobbler.com/2.0/")
                        .putParameter("api_key", authenticator.apiKey)
                        .putParameter("sk", authenticator.sessionKey)
                        .putParameter("format", "json")
                        .putParameter("artist", artist)
                        .putParameter("track", name)
                        .putParameter("method", "track.addTags")
                        .putParameter("tags", tags.joinToString(","))

                val response = (Parser().parse(rawValue = StringBuilder(
                        https.post("method", "artist", "track", "tags", "api_key", "sk", "format"))) as JsonObject)

                if (response.containsKey("error")) {
                    val msg = response.string("message")
                    throw when (response.int("error")) {
                        4 -> if (msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                        6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                        10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                        26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                        29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                        else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                    }
                } else {
                    Logger.write("track.addTags was successful!")
                }
            }
        }

        /**
         * @brief Implements the track.addTags method. Requires client authentication.
         * @param authenticator An [Authenticator] with session key.
         * @param track The [Track](https://github.com/evanperryg/lastkt/wiki/Track) whose tags we want to add to.
         * @param tags A comma-delimited list of tags to write to the specified track.
         */
        fun addTags(authenticator: Authenticator, track: Track, vararg tags: String) {
            if ((track.artist?.name == null) || (track.name == null))
                throw LastktArgumentException("given track:Track does not have non-null entries for artist name and/or track name!")
            addTags(authenticator = authenticator, artist = track.artist.name, name = track.name, tags = *tags)
        }

        /**
         * @brief Implements the track.getInfo method with standard string inputs. Does not require client authentication.
         * <p>
         *     If a musicbrainz id is provided in the mbid parameter, the name and artist parameters are optional.
         *     Likewise, if name and artist are both provided, then mbid is optional.
         * </p>
         * <p>
         *     If a value for username is specified, the Track.userLoved and Track.userPlayCount fields will be populated
         *     with information based on that user's last.fm history. If no value for username is specified, but a
         *     username is specified in the Authenticator, Track.userLoved and Track.userPlayCount will show data for
         *     the account specified there. If the username field is unspecified, and the Authenticator has no specified
         *     username, Track.userLoved and Track.userPlayCount will be left empty.
         * </p>
         * @param authenticator Any [Authenticator].
         * @param artist The artist's name for the track we want to retrieve info for.
         * @param name The name of the track we want to retrieve info for.
         * @param mbid The musicbrainz ID code for the track we want to retrieve info for.
         * @param autocorrect 1 or 0 for on or off. It is highly recommended to leave this on.
         * @param username A last.fm user to find this track's play history for.
         * @return A [Track](https://github.com/evanperryg/lastkt/wiki/Track) containing the API response data.
         */
        fun getInfo(authenticator: Authenticator, artist: String? = null, name: String? = null, mbid: String? = null, autocorrect: Int = 1, username: String? = null): Track {
            if ((name == null || artist == null) && (mbid == null))
                throw LastktArgumentException("Both name AND artist must be supplied, OR mbid may be provided, making name and artist each optional.")

            val https = Https("https://ws.audioscrobbler.com/2.0/")
                    .putParameter("method", "track.getInfo")
                    .putParameter("autocorrect", "$autocorrect")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("format", "json")

            val passParams = mutableListOf<String>("api_key", "autocorrect", "method", "format")

            var outUser: User? = null
            if (authenticator.username != null) {
                https.putParameter("username", authenticator.username!!)
                passParams.add("username")
                outUser = User(name = authenticator.username!!)
            }

            mapOf(Pair("track", name), Pair("artist", artist), Pair("mbid", mbid), Pair("username", username))
                    .entries.forEach { (a, b) ->
                if (b != null) {
                    if (b.isNotEmpty()) {
                        https.putParameter(a, b)
                        passParams.add(a)
                        if (a == "username")
                            outUser = User(name = b)
                    }
                }
            }

            val response = (Parser().parse(rawValue = StringBuilder(https.post(*passParams.toTypedArray()))) as JsonObject)
            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if (msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("track")   // if there isn't a title object, an exception was already thrown elsewhere
            val wikiBlock: JsonObject? = json?.obj("wiki")
            val albumBlock: JsonObject? = json?.obj("album")
            val artistBlock: JsonObject? = json?.obj("artist")

            return Track(name = json?.string("name"),
                    artist = Artist(name = artistBlock?.string("name"),
                            mbid = if (artistBlock?.string("mbid").isNullOrEmpty()) null
                            else artistBlock?.string("mbid"),
                            url = artistBlock?.string("url")),
                    album = Album(title = albumBlock?.string("title"),
                            url = albumBlock?.string("url"),
                            artist = Artist(name = albumBlock?.string("artist")),
                            image = Image.jsonArrayToImageList(albumBlock?.array<JsonObject>("image"))),
                    mbid = if (json?.string("mbid").isNullOrEmpty()) null
                    else json?.string("mbid"),
                    duration = json?.string("duration")?.toLong()?.div(1000),
                    url = json?.string("url"),
                    listeners = json?.string("listeners")?.toLong(),
                    playCount = json?.string("playcount")?.toLong(),
                    user = outUser,
                    userLoved = json?.string("userloved")?.let { it.toLong() > 0 },
                    userPlayCount = (json?.string("userplaycount")?.toLong() ?: if(outUser == null) null else 0L),
                    tags = Tag.jsonArrayToTagList(json?.obj("toptags")?.array("tag")),
                    wikiPublished = stringToDate(wikiBlock?.string("published")),
                    wikiSummary = wikiBlock?.string("summary"),
                    wikiContent = wikiBlock?.string("content"))
        }

        /**
         * @param authenticator Any [Authenticator].
         * @param track a [Track](https://github.com/evanperryg/lastkt/wiki/Track) containing at least a non-null artist name and track name, or at least a non-null mbid.
         * @return A [Track](https://github.com/evanperryg/lastkt/wiki/Track) containing the API response data.
         */
        fun getInfo(authenticator: Authenticator, track: Track): Track {
            if (track.mbid != null)
                return getInfo(authenticator, mbid = track.mbid)
            else if ((track.artist?.name != null) && (track.name != null))
                return getInfo(authenticator, track.artist.name, track.name)

            throw LastktArgumentException("Given Track has insufficient parameters to make a track.getInfo request! " +
                    "<track.mbid = ${track.mbid} track.artist.name = ${track.artist?.name} track.name = ${track.name}>")

        }

        /**
         * @brief Implements last.fm's track.love method. Requires client authorization.
         * @param authenticator Authenticator with client authorization.
         * @param name String The name of the track to love.
         * @param artist String The artist of the track to love.
         */
        fun love(authenticator: Authenticator, name: String, artist: String) {
            if (authenticator is NoAuthentication)
                throw LastktArgumentException("track.love requires client authorization, given Authenticator does not have client authorization!")

            val https = Https("https://ws.audioscrobbler.com/2.0/")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("artist", artist)
                    .putParameter("method", "track.love")
                    .putParameter("sk", authenticator.sessionKey)
                    .putParameter("track", name)
                    .putParameter("api_sig", md5("api_key${authenticator.apiKey}artist${artist}methodtrack.lovesk${authenticator.sessionKey}track$name${authenticator.apiSecret}"))
                    .putParameter("format", "json")
            val response = (Parser().parse(rawValue = StringBuilder(
                    https.post("method", "artist", "track", "api_key", "sk", "api_sig", "format"))) as JsonObject)
            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if (msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }
        }

        /**
         * @brief Implements the track.removeTag method. Requires client authentication.
         * @param authenticator Authenticator with client authorization.
         * @param artist The artist's name for the track we want to tag.
         * @param track The name of the track we want to tag.
         * @param tag The tag to remove from the specified track.
         */
        fun removeTag(authenticator: Authenticator, artist: String, track: String, tag: String) {
            if (authenticator is NoAuthentication)
                throw LastktArgumentException("track.addTags requires authentication, given Authenticator does not have authentication!")

            val https = Https("https://ws.audioscrobbler.com/2.0/")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("sk", authenticator.sessionKey)
                    .putParameter("format", "json")
                    .putParameter("artist", artist)
                    .putParameter("track", track)
                    .putParameter("method", "track.removeTag")
                    .putParameter("tag", tag)
            val response = (Parser().parse(rawValue = StringBuilder(
                    https.post("method", "artist", "track", "tag", "api_key", "sk", "format"))) as JsonObject)
            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if (msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }
        }

        /**
         * @brief Scrobble a track to a last.fm client account. Requires client authentication.
         *
         * The function will automatically generate a timestamp if one is not available. It will first attempt to
         * make a timestamp by subtracting the current UTC time from the duration of the track. If the track does
         * not have a duration listed, the function will generate a timestamp by subtracting 4 minutes and 1 second
         * from the current UTC time.
         *
         * @param authenticator Authenticator with client authorization.
         * @param track The [Track] to scrobble.
         * @return true if the scrobbling attempt was successful.
         */
        fun scrobble(authenticator: Authenticator, track: Track): Boolean {
            if (authenticator is NoAuthentication)
                throw LastktArgumentException("track.scrobble requires authentication, given Authenticator does not have authentication!")
            if ((track.artist?.name == null) || (track.name == null))
                throw LastktArgumentException("track.scrobble requires an artist name and track name!")

            val http = Http("http://ws.audioscrobbler.com/2.0/")

            val timeStamp = if (track.timestamp != null) track.timestamp.toString()
            else (if (track.duration != null) ((System.currentTimeMillis().div(1000)) - track.duration).toString()
            else System.currentTimeMillis().div(1000).minus(241).toString())

            // method, track, artist, timestamp and chosenByUser will be passed, so put them in immediately
            http.putParameter("api_key", authenticator.apiKey)
                    .putParameter("method", "track.scrobble")
                    .putParameter("track", track.name)
                    .putParameter("artist", track.artist.name)
                    .putParameter("timestamp", timeStamp)
                    .putParameter("sk", authenticator.sessionKey)
                    .putParameter("format", "json")
                    .putParameter("api_sig", md5("api_key${authenticator.apiKey}artist${track.artist.name}" +
                            "methodtrack.scrobblesk${authenticator.sessionKey}timestamp${timeStamp}track${track.name}${authenticator.apiSecret}"))
            val response = (Parser().parse(rawValue = StringBuilder(
                    http.post("method", "api_key", "track", "artist", "timestamp", "sk", "api_sig", "format")))
                    as JsonObject)
            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if (msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            return response.obj("scrobbles")?.obj("@attr")?.int("accepted") == 1
        }

        /**
         * @brief Scrobble a track to a last.fm client account. Requires client authentication.
         *
         * The function will automatically generate a timestamp if one is not available. It will first attempt to
         * make a timestamp by subtracting the current UTC time from the duration of the track. If the track does
         * not have a duration listed, the function will generate a timestamp by subtracting 4 minutes and 1 second
         * from the current UTC time.
         *
         * @param authenticator Authenticator with client authorization.
         * @param name The name of the track to scrobble. Optional if [mbid] is present.
         * @param artist The artist name of the track to scrobble. Optional if [mbid] is present.
         * @param mbid The MusicBrainz ID for the track to scrobble. Optional if both [name] and [artist] are present.
         * @return true if the scrobbling attempt was successful.
         */
        fun scrobble(authenticator: Authenticator, name: String? = null, artist: String? = null, mbid: String? = null): Boolean =
            scrobble(authenticator, Track(name = name, artist = Artist(name = artist), mbid = mbid, url = null))

        /**
         * @brief Implements last.fm's track.unlove method. Requires authentication.
         * @param authenticator Authenticator with client authorization.
         * @param name The song name.
         * @param artist The name of the artist.
         */
        fun unlove(authenticator: Authenticator, name: String, artist: String) {
            if (authenticator is NoAuthentication)
                throw LastktArgumentException("track.unlove requires client authorization, given Authenticator does not have client authorization!")
            val https = Https("https://ws.audioscrobbler.com/2.0/")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("artist", artist)
                    .putParameter("method", "track.unlove")
                    .putParameter("sk", authenticator.sessionKey)
                    .putParameter("track", name)
                    .putParameter("api_sig", md5("api_key${authenticator.apiKey}artist${artist}methodtrack.unlovesk${authenticator.sessionKey}track$name${authenticator.apiSecret}"))
                    .putParameter("format", "json")
            val response = (Parser().parse(rawValue = StringBuilder(
                    https.post("method", "artist", "track", "api_key", "sk", "api_sig", "format"))) as JsonObject)
            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if (msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }
        }

        /**
         * @brief Update the 'Now Playing' track on the last.fm client account. Requires client authentication.
         * @param authenticator Authenticator with client authorization.
         * @param track The [Track] to set as now playing.
         */
        fun updateNowPlaying(authenticator: Authenticator, track: Track) {
            if (authenticator is NoAuthentication)
                throw LastktArgumentException("track.updateNowPlaying requires authentication, given Authenticator does not have authentication!")
            if ((track.artist?.name == null) || (track.name == null))
                throw LastktArgumentException("track.updateNowPlaying requires an artist name and track name!")

            val http = Http("http://ws.audioscrobbler.com/2.0/")

            // method, track, artist, timestamp and chosenByUser will be passed, so put them in immediately
            http.putParameter("api_key", authenticator.apiKey)
                    .putParameter("method", "track.updateNowPlaying")
                    .putParameter("track", track.name)
                    .putParameter("artist", track.artist.name)
                    .putParameter("sk", authenticator.sessionKey)
                    .putParameter("format", "json")
                    .putParameter("api_sig", md5("api_key${authenticator.apiKey}artist${track.artist.name}" +
                            "methodtrack.updateNowPlayingsk${authenticator.sessionKey}track${track.name}${authenticator.apiSecret}"))

            val response = (Parser().parse(rawValue = StringBuilder(
                    http.post("method", "api_key", "track", "artist", "sk", "api_sig", "format")))
                    as JsonObject)

            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    4 -> if (msg != null) LastktAuthenticationFailedException(msg) else LastktAuthenticationFailedException()
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }
        }

        /**
         * @brief Update the 'Now Playing' track on the last.fm client account. Requires client authentication.
         * @param authenticator Authenticator with client authorization.
         * @param name The name of the track to set as now playing. Optional if [mbid] is present.
         * @param artist The artist name of the track to set as now playing. Optional if [mbid] is present.
         * @param mbid The MusicBrainz ID for the track to set as now playing. Optional if both [name] and [artist] are present.
         */
        fun updateNowPlaying(authenticator: Authenticator, name: String? = null, artist: String? = null, mbid: String? = null) =
                updateNowPlaying(authenticator, Track(name = name, artist = Artist(name = artist), mbid = mbid, url = null))

        /**
         * @brief Return a list of similar tracks to the given track, with values indicating the degree of similarity.
         * @param authenticator Authenticator with client authorization.
         * @param name The name of the track to find similar tracks for. Optional if [mbid] is provided.
         * @param artist The name of the track's artist. Optional if [mbid] is provided.
         * @param mbid The MusicBrainz ID for the track. Optional if both [name] and [artist] are provided.
         * @param limit The maximum number of results to be returned. Default is 100. Last.fm will never return more than 250 results.
         * @param autocorrect Set this to 0 to disable Last.fm's autocorrecting utility. It is strongly recommended to leave this at default (1)
         * @return A List of Pair<Long,Track>. The float is a number from 0-1 representing the degree of similarity between the track given by the request, and the track in this pair.
         */
        fun getSimilar(authenticator: Authenticator, name: String? = null, artist: String? = null, mbid: String? = null, limit: Int = 100, autocorrect: Int = 1): List<Pair<Float,Track>> =
            if(((name == null) || (artist == null)) && (mbid == null))
                throw LastktArgumentException("Both name AND artist must be supplied, OR mbid may be provided, making track and artist each optional.")
            else {
            val https = Https("https://ws.audioscrobbler.com/2.0/")
                    .putParameter("method", "track.getSimilar")
                    .putParameter("autocorrect", "$autocorrect")
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("limit", "$limit")
                    .putParameter("format", "json")

            val passParams = mutableListOf<String>("api_key", "autocorrect", "method", "limit", "format")

            mapOf(Pair("track", name), Pair("artist", artist), Pair("mbid", mbid))
                    .entries.forEach { (a, b) ->
                if (b != null) {
                    if (b.isNotEmpty()) {
                        https.putParameter(a, b)
                        passParams.add(a)
                    }
                }
            }

            val response = (Parser().parse(rawValue = StringBuilder(https.post(*passParams.toTypedArray()))) as JsonObject)
            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val json = response.obj("similartracks")?.array<JsonObject>("track")
            val out = mutableListOf<Pair<Float,Track>>()
            json?.forEach {
                val match: Float = it.float("match") ?: 0.0F
                val artst = it.obj("artist")

                out.add(Pair(match, Track(name = it.string("name"),
                        playCount = it.long("playcount"),
                        mbid = if(it.string("mbid").isNullOrEmpty()) null else it.string("mbid"),
                        artist = Artist(name = artst?.string("name"),
                                mbid = if(artst?.string("mbid").isNullOrEmpty()) null else artst?.string("mbid"),
                                url = artst?.string("url")),
                        url = it.string("url"),
                        duration = it.long("duration"))))

            }
            out
        }

        /**
         * @brief Return a list of similar tracks to the given track, with values indicating the degree of similarity.
         * @param authenticator Any [Authenticator].
         * @param track The [Track] to find similar tracks for.
         * @param limit The maximum number of results to be returned. Default is 100. Last.fm will never return more than 250 results.
         * @param autocorrect Set this to 0 to disable Last.fm's autocorrecting utility. It is strongly recommended to leave this at default (1)
         * @return A List of Pair<Long,Track>. The float is a number from 0-1 representing the degree of similarity between the track given by the request, and the track in this pair.
         */
        fun getSimilar(authenticator: Authenticator, track: Track, limit: Int = 100, autocorrect: Int = 1): List<Pair<Float,Track>> =
                Track.getSimilar(authenticator, track.name, track.artist?.name, track.mbid, limit, autocorrect)

        /**
         * @brief Perform the Track.search API method, using the given parameters. Takes paginated input range.
         * @param authenticator Any [Authenticator].
         * @param name The name of the track to search for.
         * @param artist The name of the track's artist. Optional.
         * @param limit The maximum number of results returned.
         * @param page The page of results to be returned. 1 is the first page.
         */
        fun search(authenticator: Authenticator, name: String, artist: String? = null, limit: Int = 30, page: Int = 1): List<Track> {
            val https = Https("https://ws.audioscrobbler.com/2.0/")
                    .putParameter("method", "track.search")
                    .putParameter("track", name)
                    .putParameter("api_key", authenticator.apiKey)
                    .putParameter("limit", "$limit")
                    .putParameter("page", "$page")
                    .putParameter("format", "json")

            val passParams = mutableListOf<String>("api_key", "track", "method", "limit", "page", "format")

            artist.doIfNotNull {
                https.putParameter("artist", it)
                passParams.add("artist")
            }

            val response = (Parser().parse(rawValue = StringBuilder(https.post(*passParams.toTypedArray()))) as JsonObject)
            if (response.containsKey("error")) {
                val msg = response.string("message")
                throw when (response.int("error")) {
                    6 -> if (msg != null) LastktInvalidParametersException(msg) else LastktInvalidParametersException()
                    10 -> if (msg != null) LastktInvalidAPIKeyException(msg) else LastktInvalidAPIKeyException()
                    26 -> if (msg != null) LastktAPIKeySuspendedException(msg) else LastktAPIKeySuspendedException()
                    29 -> if (msg != null) LastktRateLimitedException(msg) else LastktRateLimitedException()
                    else -> LastktConnectionException("Last.FM responded with error message ${response.toJsonString()}")
                }
            }

            val out = mutableListOf<Track>()
            val json = response.obj("results")?.obj("trackmatches")?.array<JsonObject>("track")

            json?.forEach {
                out.add(Track(name = it.string("name"),
                                artist = Artist(name = it.string("artist")),
                                url = it.string("url"),
                                mbid = if (it.string("mbid").isNullOrEmpty()) null else it.string("mbid"),
                                listeners = it.string("listeners")?.toLong()))
            }

            return out
        }

        /**
         * @brief Perform the Track.search API method, but with non-paginated input parameters.
         * @param authenticator Any [Authenticator].
         * @param name The name of the track to search for.
         * @param artist The name of the track's artist. Optional.
         * @param startIndex The index of the first result to include in the returned list.
         * @param endIndex One plus the index of the last result to include in the returned list.
         */
        fun searchUnpaginated(authenticator: Authenticator, name: String, artist: String? = null, startIndex: Int, endIndex: Int) =
                search(authenticator, name, artist, endIndex + 1, 1).subList(startIndex, endIndex)
    }

    /**
     * @brief Perform the last.fm track.getInfo method using the data stored in this [Track]. Does not use client authentication.
     *
     * If this [Track]'s [user] property is not null, the returned
     * Track's [user], [userLoved] and [userPlayCount] properties will pertain to this Track's [user] property. If this
     * Track's [user] property is null, the returned Track's [user], [userLoved] and [userPlayCount] properties will
     * pertain to the username found in [authenticator]. If no last.fm username is found in this or [authenticator], the
     * returned Track's [user], [userLoved] and [userPlayCount] properties will be null.
     *
     * @param authenticator Any Authenticator.
     * @return Track
     */
    fun getInfo(authenticator: Authenticator): Track = Track.getInfo(authenticator, this)

    /**
     * @brief Iterate through each of the track's tags. Makes 0 API requests.
     * @throws LastktStateException Thrown if Track.tags is null.
     * @param action Function1<Tag, Unit>
     * @return Unit
     */
    inline fun forEachTag(action: (Tag) -> Unit) {
        if (tags == null) throw LastktStateException("Track.forEachTag cannot be called on an Track with a null tags property!")
        else return tags.forEach(action)
    }

    /**
     * @brief Iterate through tracks similar to this track. Makes 1 API request.
     * @param authenticator Any [Authenticator].
     * @param limit A maximum number of tracks to return. Default is 100. Last.fm will never return more than 250 results.
     * @param autocorrect Set this to 0 to disable Last.fm's autocorrect utility. It is recommended to just leave this as is default value.
     */
    inline fun forEachSimilarTrack(authenticator: Authenticator, limit: Int = 100, autocorrect: Int = 1, action: (Float, Track) -> Unit ) {
        return getSimilar(authenticator, this, limit, autocorrect).forEach{ action(it.first, it.second) }
    }

}