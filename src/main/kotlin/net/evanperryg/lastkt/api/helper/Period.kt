package net.evanperryg.lastkt.api.helper

import net.evanperryg.lastkt.exception.LastktArgumentException

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date   5/14/2018
 * @brief  Enumeration of the "period" parameter used by some last.fm API methods.
 */
enum class Period(val string: String) {
    WEEK("7day"),
    MONTH("1month"),
    THREEMONTH("3month"),
    SIXMONTH("6month"),
    YEAR("12month"),
    OVERALL("overall");

    fun convert(string: String): Period =
            when(string) {
                WEEK.string -> WEEK
                MONTH.string -> MONTH
                THREEMONTH.string -> THREEMONTH
                SIXMONTH.string -> SIXMONTH
                YEAR.string -> YEAR
                OVERALL.string -> OVERALL
                else -> throw LastktArgumentException("given string does not correspond to a Period enum element!")
            }
}