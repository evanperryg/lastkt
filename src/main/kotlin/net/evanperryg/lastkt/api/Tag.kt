package net.evanperryg.lastkt.api

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date   5/13/2018
 * @brief  A data class representation of a single last.fm tag.
 * @param name The tag's name
 * @param url  A url to the last.fm page for this tag.
 */
data class Tag(val name: String, val url: String) {
    companion object {

        /**
         * @brief    Convert a last.fm JSON tag element into a [Tag].
         * @property tagObject One object in a tag array from a last.fm API response.
         * @return   A Tag instance containing tha name and url determined from [tagObject]
         */
        fun jsonToTag(tagObject: JsonObject): Tag {
            return Tag(name = tagObject.string("name")!!,
                       url = tagObject.string("url")!!)
        }

        /**
         * @brief    Convert a JSON array of last.fm JSON tag elements into a [kotlin.collections.List] of [Tag]s.
         * @property jsonArray A JsonArray of tags from a last.fm API response.
         * @return   A [kotlin.collections.List] of [Tag]s derived from [jsonArray]
         */
        fun jsonArrayToTagList(jsonArray: JsonArray<JsonObject>?): List<Tag> {
            if(jsonArray == null) return listOf()
            val out = mutableListOf<Tag>()
            jsonArray.forEach{ jsonToTag(it).let{
                out.add(it)
            } }
            return out.toList()
        }
    }

}