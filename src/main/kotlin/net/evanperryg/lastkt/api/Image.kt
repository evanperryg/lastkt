package net.evanperryg.lastkt.api

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @brief A data model representing an image element in the returned content of a last.fm API request.
 * @author Evan Perry Grove
 * @date   5/13/2018
 * @property url  The url where the image can be accessed
 * @property size A string representing the relative size of the image, e.g. "large"
 */
data class Image(val url: String?,
                 val size: String?) {
    companion object {

        /**
         * @brief Converts a JSON array of last.fm JSON image elements into a List of Images.
         * @param imageArray a JSON Array of last.fm image elements.
         * @return A [List][kotlin.collections.List] of [Image]s
         */
        fun jsonArrayToImageList(imageArray: JsonArray<JsonObject>?): List<Image> {
            if(imageArray == null) return listOf()
            val out = mutableListOf<Image>()
            imageArray.forEach{
                out.add(Image(url = it.string("#text"),
                              size = it.string("size")))
            }
            return out
        }
    }
}