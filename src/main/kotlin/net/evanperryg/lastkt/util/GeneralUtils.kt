package net.evanperryg.lastkt.util

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date   5/13/2018
 */

/**
 * Used to create a lambda which will only run if the given nullable is not null.
 * Adapted from the Kotlin forums.
 */
fun <T: Any?> T?.doIfNotNull(f: (it: T)-> Unit){
    if (this != null) f(this)
}


/**
 * Used to create a lambda which will only run if the given nullable is null.
 * Adapted from the Kotlin forums.
 */
fun <T: Any?> T?.doIfNull(f: ()-> Unit){
    if (this == null) f()
}

/**
 * Destructures pairs for use in an iterating lambda
 * Adapted from the Kotlin forums.
 */
inline fun <A, B> Iterable<Pair<A, B>>.forEachPair(action: (A, B) -> Unit) =
        forEach { action(it.first, it.second) }