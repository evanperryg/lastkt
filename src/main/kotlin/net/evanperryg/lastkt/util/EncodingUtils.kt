package net.evanperryg.lastkt.util

import java.math.BigInteger
import java.net.URLEncoder
import java.security.MessageDigest

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date   5/11/2018
 */

/**
 * @return an MD5 hash of the input String
 */
fun md5(str: String): String {
    val digest: MessageDigest? = MessageDigest.getInstance("MD5")
    val byteArray = str.toByteArray(Charsets.UTF_8)
    val hash = BigInteger(1, digest!!.digest(byteArray))
    return hash.toString(16).padStart(32, '0')
}

/**
 * Breakout for the UTF-8 encoder, may allow for compensation for some inconsistencies (e.g. + vs %20)
 */
fun makeUTF8(str: String?): String = URLEncoder.encode(str, "UTF-8")