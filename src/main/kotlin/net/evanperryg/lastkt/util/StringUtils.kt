package net.evanperryg.lastkt.util

import java.text.SimpleDateFormat
import java.util.*

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date   5/12/2018
 */

fun removeAllWhitespaceNotInQuotes(string: String): String {
    val regx = "\\s+(?=((\\\\[\\\\\"]|[^\\\\\"])*\"(\\\\[\\\\\"]|[^\\\\\"])*\")*(\\\\[\\\\\"]|[^\\\\\"])*$)".toRegex()
    return string.replace(regx, "")
}

/** Converts Last.fm's date/time format to java.lang.Date */
fun stringToDate(string: String?, locale: Locale = Locale.ENGLISH): Date? {
    if(string == null) return null
    if(string.isEmpty()) return null
    val foo = SimpleDateFormat("dd MMM yyyy, HH:mm")
    return foo.parse(string)
}