package net.evanperryg.lastkt.auth

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import net.evanperryg.lastkt.http.Https
import net.evanperryg.lastkt.util.Logger
import net.evanperryg.lastkt.util.md5
import java.io.File
import kotlin.coroutines.experimental.suspendCoroutine

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date   11 May 2018
 */
class MobileAuthentication(override val username: String,
                           password: String,
                           override val apiKey: String,
                           override val apiSecret: String): Authenticator {
    private val https: Https = Https("https://ws.audioscrobbler.com/2.0/")
            .putParameter("method","auth.getMobileSession")
            .putParameter("api_key", apiKey)
            .putParameter("password", password)
            .putParameter("username", username)
            .putParameter("format", "json")
            .putParameter("api_sig",md5("api_key${apiKey}methodauth.getMobileSessionpassword${password}username${username}$apiSecret"))
    private val sessionResponse: JsonObject = Parser()
            .parse(StringBuilder(https.post("method", "username", "password", "api_key", "api_sig", "format"))) as JsonObject
    override val sessionKey = sessionResponse.obj("session")!!.string("key")!!
    override val isSubscriber: Boolean = sessionResponse.obj("session")?.int("subscriber") == 1
    init {
        https.putParameter("sk", sessionKey)
        Logger.write("Mobile Authentication for username $username complete.")
    }

    fun writeToFile(filePath: String) {
        File(filePath).printWriter().use {
            out ->
            out.println("{\n" +
                    "    \"apiKey\": \"$apiKey\",\n" +
                    "    \"apiSecret\": \"$apiSecret\",\n" +
                    "    \"username\": \"$username\",\n" +
                    "    \"sessionKey\": \"$sessionKey\",\n" +
                    "    \"isSubscriber\": $isSubscriber\n" +
                    "}")
        }
    }

}