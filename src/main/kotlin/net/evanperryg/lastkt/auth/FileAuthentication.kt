package net.evanperryg.lastkt.auth

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import net.evanperryg.lastkt.exception.LastktStateException
import net.evanperryg.lastkt.util.Logger

class FileAuthentication(filePath: String): Authenticator {
    private val json = (Parser().parse(fileName = filePath) as JsonObject)
    override val username = json.string("username")
    override val apiKey = json.string("apiKey")
            ?: throw LastktStateException("API key could not be found in file $filePath")
    override val apiSecret = json.string("apiSecret")
            ?: throw LastktStateException("API secret could not be found in file $filePath")
    override val sessionKey = json.string("sessionKey")
            ?: throw LastktStateException("Session key could not be found in file $filePath!")
    override val isSubscriber = json.boolean("isSubscriber")
            ?: false    // TODO: if streaming support is added, make this throw an exception
    init {
        Logger.write("FileAuthentication for user $username completed from $filePath")
    }

}