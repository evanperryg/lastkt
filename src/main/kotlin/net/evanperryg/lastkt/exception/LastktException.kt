package net.evanperryg.lastkt.exception

/*
 * Copyright 2018 Evan Perry Grove
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Evan Perry Grove
 * @date   5/13/2018
 * @brief  Exceptions used by lastkt are stored here.
 * The justification for using a special set of exceptions for Lastkt is simple: As long as the user isn't calling
 * these exceptions themselves, these exceptions will only occur as a result of something that went wrong with
 * the user's usage of methods/classes in Lastkt. As a result, it is easier to narrow down the locaton/cause
 * of an exception.
 */

/**
 * @brief Parent class for all other exceptions defined in `LastktException.kt`
 */
open class LastktException(override val message: String): Exception(message)

/**
 * @brief Thrown when an invalid argument, or an invalid combination of arguments, is passed.
 */
class LastktArgumentException(override val message: String): LastktException(message)

/**
 * @brief Thrown when an attempted Last.fm API request returns an unknown error.
 */
class LastktConnectionException(override val message: String): LastktException(message)

/**
 * @brief Thrown when an erroneous state occurs in a lastkt method.
 */
class LastktStateException(override val message: String): LastktException(message)

/**
 * @brief Thrown when a Last.FM API request returns "error 4: Authentication Failed"
 */
class LastktAuthenticationFailedException(override val message: String = "Last.fm Error 4: Authentication Failed - You do not have permissions to access the service"): LastktException(message)

/**
 * @brief Thrown when a Last.FM API request returns "error 6: Invalid Parameters"
 */
class LastktInvalidParametersException(override val message: String = "Last.fm Error 6: Invalid Parameters - Your request is missing a required parameter"): LastktException(message)

/**
 * @brief Thrown when a Last.FM API request returns "error 10: Invalid API Key"
 */
class LastktInvalidAPIKeyException(override val message: String = "Last.fm Error 10: Invalid API key - You must be granted a valid key by last.fm"): LastktException(message)

/**
 * @brief Thrown when a Last.FM API request returns "error 26: API Key Suspended"
 */
class LastktAPIKeySuspendedException(override val message: String = "Last.fm Error 26: API Key Suspended - This application is not allowed to make requests to the web services"): LastktException(message)

/**
 * @brief Thrown when a Last.FM API request returns "error 29: Rate Limit Exceeded"
 */
class LastktRateLimitedException(override val message: String = "Last.fm Error 29: Rate Limit Exceded - Your IP has made too many requests in a short period, exceeding our API guidelines"): LastktException(message)
