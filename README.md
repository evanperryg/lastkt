[![Generic badge](https://img.shields.io/badge/License-Apache2.0-blue.svg)](https://github.com/evanperryg/lastkt/blob/master/LICENSE)

## Getting Started
- [The Wiki](https://github.com/evanperryg/lastkt/wiki) includes sample code and the "API Cheat Sheet" which shows the exact contents of the data structures returned by the API breakout methods.

- [The Full Documentation](https://evanperryg.github.io/lastkt/lastkt/index.html) has more detailed information on the entire library.


## Adding last.kt to a Gradle Project
build.gradle:
```gradle
repositories {
  maven { url "https://jitpack.io" }
}

dependencies {
    compile 'com.gitlab.evanperryg:lastkt:+'
}
```
where `+` is the version number of the desired release of lastkt.

**Latest Stable Release:** [![GitHub release](https://img.shields.io/github/release/evanperryg/lastkt.svg)](https://github.com/evanperryg/lastkt/releases)
